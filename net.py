import os
import time

class net:

    PINGHOST = "http://www.google.com"

    @staticmethod
    def connect():
        time.sleep(20)
        # hot plug all usb devices
        os.system("sudo udevadm trigger --subsystem-match=usb")
        time.sleep(10)
        # dial up
        os.system("sudo wvdial")
        time.sleep(20)
        

    @staticmethod
    def ping():
        # ping the host check network status
        response = os.system("sudo ping -c 1 " + PINGHOST)
        return (response == 0) 
