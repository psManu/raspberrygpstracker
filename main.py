import hmc5883
import neo6m
import time


# if onContinuous mode data is being transmitted to backend via socket continuously
# it is set True/False by backend with a message to device via same socket
isOnContineousMode = False
webSocket = webSocket("url", onConnect, onDisconnect, onMessage)
isWebSocketOpen = False

tracker = neo6m()
compass = hmc5883()

compass.setContinuousMode()
compass.setDeclination(-2.0, 8.0)

# at the begining keep re-dialing until the network connection is established
while not net.ping():
    net.connect()

webSocket.connect()

def main():
    # network drop is detected by webSocket onDisconnect() event 
    # this main method contains all driving functionalities
    # always the main thread must return back to main()   
        
    try:  
        sendData()
                        
    except Exception as e:
        exceptionHandler(e)


def exceptionHandler(ex):
    try:
        # handle ex
        main()
    except Exception as e:
        # log(e)
        # only way to come out of this is trying a reboot 

def onConnect():
    isWebSocketOpen = True
    main()

def onMessage(data):
    # when backend requests data it should call on this socket
    if(data.continuousMode):
        isOnContineousMode = True
    main()

def onDisconnect():

    while not net.ping():
        net.connect()
    webSocket.connect()
    onDisconnect()        

def sendData():

    # eternal loop!
    # in happy path where network is up and socket com is active this loop will iterate eteranally.
    # unless server send a message via socket, upon which the execution will go to onMessage()
    # after processing that message it will get back to main() and ultimately come here 
    while isOnContineousMode:
        try:
            #get gps and compass data
            #send via socket
            gps = tracker.getGps()
            heading = compass.getHeading()

            if(gps and heading):
                # socket push 

        except Exception ex:
            raise ex


# main entry point
# but highly unlikely to be called
main()