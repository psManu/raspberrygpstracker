import serial
import re
import datetime

KNOTSTOKMPH = float(1.852)
COMPORTADDRESS = "/dev/ttyAMA0"
CHECKSUMREGEX = re.compile('\*[A-F0-9]{2}')
SERIALCOM = serial.Serial()
SERIALCOM.port = COMPORTADDRESS
SERIALCOM.open()

class neo6m(object):

    #checksum validation for transmission errors
    def checksum(self, sentence):
        cksum = CHECKSUMREGEX.findall(sentence)[0][1:]
        cksumData = re.sub("(\n|\r\n)", "", sentence[sentence.find("$") + 1 : sentence.find("*")])
        csum = 0
        for c in cksumData:
            csum ^= ord(c)
        return (hex(csum) == hex(int(cksum, 16)))

    def getGps(self):
        reading = SERIALCOM.readline()
        # parsing only the gprmc(recommended minimum data) sentence since it contains the summary
        if (reading.startswith('$GPRMC') and self.checksum(reading) and len(reading) >= 20):
            gpsdata = reading.split(",")
            if(gpsdata[2] and (gpsdata[2].upper() == "A")):
                
                try:
                    # yyyy-MM-dd HH:mm:ss.SSS 
                    datetimeStamp = self.parseDateTime(gpsdata[1], gpsdata[9])
                    # in DMM format. It requires minimum processing and google maps accepts them
                    lat = self.parseLat(gpsdata[3], gpsdata[4])
                    lon = self.parseLon(gpsdata[5], gpsdata[6])
                    speed = self.parseSpeed(gpsdata[7])

                    return gps(lat, lon, speed)

                except:
                    # An exception occured while parsing.This is possible in the early minutes after a cold start
                    # because it sends incomplete data in the beginning.In such a case ignore those.
                    return None
        
        return None
    
    def parseSpeed(self, speedinKnots):
        if not speedinKnots:
            raise ValueError('Invalid speed reading : ' + speedinKnots)
        return float(speedinKnots) * KNOTSTOKMPH

    def parseDateTime(self, timeStamp, dateStamp):
        return datetime.date.today()
        
    def parseLat(self, reading, dir):
        parsed = self.parseLatLonFix(reading)        
        if dir == 'N':
            return parsed
        if dir == 'S':
            return -parsed
        raise ValueError('Invalid Latitude Direction : ' + dir)
    
    def parseLon(self, reading, dir):
        parsed = self.parseLatLonFix(reading)        
        if dir == 'E':
            return parsed
        if dir == 'W':
            return -parsed
        raise ValueError('Invalid Longitude Direction : ' + dir)
    
    def parseLatLonFix(self, reading):
        if not reading or reading == '0':
            raise ValueError('Invalid coordinate reading : ' + reading)
        d, m = re.match(r'^(\d+)(\d\d\.\d+)$', reading).groups()
        return float(d) + float(m) / 60



class gps(object):

    def __init__(self, lat, lon, speed):
        self.lat = lat
        self.lon = lon
        self.speed = speed




